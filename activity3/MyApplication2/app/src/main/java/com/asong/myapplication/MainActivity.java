package com.asong.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText textfield= findViewById(R.id.et_input);
        Button btn= findViewById(R.id.bt_show);
        textfield.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String text= textfield.getText().toString();
                if(text.isEmpty()){
                    alert("Please input data!");
                }
                else{
                    alert(text);
                }
            }
        });
    }
    private void alert(String message){
        AlertDialog  dialog= new AlertDialog.Builder(MainActivity.this)
                .setTitle("Message")
                .setMessage(message)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .create();
        dialog.show();

    }
}